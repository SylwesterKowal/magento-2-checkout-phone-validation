<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\CheckoutPhoneValidation\Plugin\Frontend\Magento\Checkout\Block\Checkout;

class AttributeMerger
{

    public function afterMerge(
        \Magento\Checkout\Block\Checkout\AttributeMerger $subject,
        $result
    ) {
        $result['telephone']['validation'] = [
            'required-entry'  => true,
            'min_text_length' => 9,
            'validate-number' => true
        ];
        $result['street']['children'][0]['validation'] = [
            'required-entry'  => true,
            'validate-adres' => true    // trzeba nadpisać plik /vendor/magento/module-ui/view/base/web/js/lib/validation/rules.js
                                        // i dodanie validacji:
                                        //        "validate-adres": [
                                        //            function(value) {
                                        //                return /[0-9]/.test(value);
                                        //            },
                                        //            $.mage.__('Your street address must contain a housenumber')
                                        //        ]
        ];
//        file_put_contents("_validate.txt",print_r($result,true));
        return $result;
    }
}

